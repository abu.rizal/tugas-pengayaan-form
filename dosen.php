<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>SIM Dosen</title>
</head>
<body>
    <?php 
        if (isset($_SESSION['message'])):
    ?>

    <div class="alert-<?=$_SESSION['msg_type']?>"></div>
    
    <?php 
        endif;
    ?>
<!-- Membuat Scaffold tampilan -->
<div class="wrapper">
            <div class="sidebar">
                <!-- Membuat tampilan menu sidebar -->
                <h2>SIM Dosen</h2>
                <hr>
                <ul>
                    <li><a href="index.php">Jadwal Kelas</a></li>
                    <li><a href="dosen.php">Dosen</a></li>
                    <li><a href="kelas.php">Kelas</a></li>
                </ul>
            </div>
            <div class="main_content">
                <div class="page_title">
                    DOSEN <!-- Title Halaman -->
                </div>
            <div class="container">
                <div class="display">
                <?php
                    // Koneksi ke Database
                    $mysqli = new mysqli('localhost', 'root', '', 'db_sidosen') or die (mysqli_error($mysqli));

                    // Menyimpan hasil query ke dalam variabel
                    $result  = $mysqli->query("SELECT * FROM dosen") or die($mysqli->error);
                ?>

                <?php
                    // perulangan untuk menampilkan isi database
                        while ($row = $result->fetch_assoc()): ?>
                        
                    <!-- Tabel untuk menampilkan hasil query (READ) -->
                    <table class="table" cellspacing="0" cellpadding="0">

                        <tr>
                            <td rowspan="5" class="image"><img src="uploads/<?php echo $row["foto_dosen"]; ?>" width="200" height="200" alt="<?php echo $row["foto_dosen"]; ?>"></td>
                        </tr>
                        <tr>
                            <td class="detail"><h4>ID Dosen</h4><?php echo $row['id_dosen']; ?></td>
                            <td class="detail"><h4>Prodi</h4><?php echo $row['prodi']; ?></td>
                        </tr>
                        <tr>
                            <td class="detail"><h4>NIP Dosen</h4><?php echo $row['nip_dosen']; ?></td>
                            <td class="detail"><h4>Fakultas</h4><?php echo $row['fakultas']; ?></td>
                        </tr>
                        <tr>
                            <td class="detail"><h4>Nama Dosen</h4><?php echo $row['nama_dosen']; ?></td>
                            <td class="detail"></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="action">
                                <!-- Tombol edit dan delete -->
                                <a href="dosen.php?edit=<?php echo $row['id_dosen']; ?>" class="edit">Sunting</a>
                                <a href="dosen.php?delete=<?php echo $row['id_dosen']; ?>" class="delete">Hapus</a>
                            </td>
                        </tr>
                
                    </table>
                    <?php 
                        endwhile; 
                    ?>

                </div>

                <!-- Form untuk input data ke database -->
                <div class="form">
                    <?php
                    $update = false;
                    $id = 0; // variable id digunakan untuk menampung nilai id(PRIMARY) awal

                    // variable awalnya ditetapkan sebagai null, agar tampilan input kosong
                    $id_dosen = '';
                    $nip = '';
                    $nama_dosen = '';
                    $prodi = '';
                    $fakultas = '';
                    $foto = '';
                    
                    // perkondisian untuk edit(UPDATE)
                    if (isset($_GET['edit'])){ // jika tombol edit/sunting ditekan,
                        $id_dosen = $_GET['edit']; // maka akan mengambil nilai dari id kemudian melakukan query:
                        $result = $mysqli->query("SELECT * FROM dosen WHERE id_dosen=$id_dosen") or die($mysqli->error);
                        
                        $update = true;
                        $row = mysqli_fetch_assoc($result);

                        // perkondisian untuk menampilkan semua data dari data yg ditekan fungsi edit nya
                        // data kemudian ditampilkan ke form update
                        if($row['id_dosen'] > 0){
                            
                            $id_dosen = $row['id_dosen'];
                            $nip = $row['nip_dosen'];
                            $nama_dosen = $row['nama_dosen'];
                            $prodi = $row['prodi'];
                            $fakultas = $row['fakultas'];
                            $foto = $row['foto_dosen'];
                            $id = $row['id_dosen'];
                        }
                    }

                    // perkondisian fungsi update
                    if (isset($_POST['update'])){
                        $id = $_POST['id'];
                        $id_dosen = $_POST['id_dosen'];
                        $nip = $_POST['nip'];
                        $nama_dosen = $_POST['nama'];
                        $prodi = $_POST['prodi'];
                        $fakultas = $_POST['fakultas'];
                        $foto = $_POST['foto_dosen'];
                        $mysqli->query("UPDATE dosen SET id_dosen='$id_dosen',foto_dosen='$foto',nip_dosen='$nip',nama_dosen='$nama_dosen',prodi='$prodi',fakultas='$fakultas' WHERE `dosen`.`id_dosen`=$id") or die($mysqli->error);

                        $_SESSION['message'] = "Data berhasil diperbarui!";
                        $_SESSION['msg_type'] = "info";

                        header("location: dosen.php");
                    }
                    ?>

                    <!-- Perkondisian untuk mengubah bentuk form dari submit form ke update form -->
                    <?php if($update == true): ?>
                    <h3>Ubah Data Dosen</h3>
                    <?php else: ?>
                    <h3>Data Dosen Baru</h3>
                    <?php endif; ?>

                    <!-- Tampilan form -->
                    <form action="dosen.php" method= "POST">
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                        <label for="id_dosen">ID Dosen :</label><br>
                        <input type="text" name="id_dosen" value="<?php echo $id_dosen; ?>"><br>

                        <label for="nip">NIP Dosen :</label><br>
                        <input type="text" name="nip" value="<?php echo $nip; ?>"><br>

                        <label for="nama">Nama Dosen :</label><br>
                        <input type="text" name="nama" value="<?php echo $nama_dosen; ?>"><br>

                        <label for="prodi">Prodi :</label><br>
                        <input type="text" name="prodi" value="<?php echo $prodi; ?>"><br>

                        <label for="fakultas">Fakultas :</label><br>
                        <input type="text" name="fakultas" value="<?php echo $fakultas; ?>"><br>
                        
                        <label for="foto">Foto :</label><br>
                        <input type="file" name="foto_dosen" value="<?php echo $foto; ?>"><br>

                        <!-- Perkondisian untuk mengubah bentuk tombol dari submit ke update -->
                        <?php if($update == true): ?>
                            <input type="submit" name="update" value="Update">
                        <?php else: ?>
                        <input type="submit" name="submit" value="Submit">
                        <?php endif; ?>
                    </form>

                    <?php

                        // perkondisian untuk input data ke dalam Database (CREATE)
                        if (isset($_POST['submit'])){
                            $id_dosen = $_POST['id_dosen'];
                            $nip = $_POST['nip'];
                            $nama_dosen = $_POST['nama'];
                            $prodi = $_POST['prodi'];
                            $fakultas = $_POST['fakultas'];
                            $foto = $_POST['foto_dosen'];

                            $mysqli->query("INSERT INTO dosen (id_dosen,foto_dosen,nip_dosen,nama_dosen,prodi,fakultas) VALUES ('$id_dosen','$foto','$nip','$nama_dosen','$prodi','$fakultas')") or
                                die($mysqli->error);
                            
                            $_SESSION['message'] = "Data berhasil disimpan!";
                            $_SESSION['msg_type'] = "success";

                            header("location: dosen.php");
                        }

                        // perkondisian untuk menghapus data dari Database (DELETE)
                        if (isset($_GET['delete'])){
                            $id_dosen = $_GET['delete'];
                            $mysqli->query("DELETE FROM dosen WHERE id_dosen=$id_dosen") or die($mysqli->error);

                            $_SESSION['message'] = "Data berhasil dihapus!";
                            $_SESSION['msg_type'] = "danger";

                            header("location: dosen.php");
                        }


                    ?>
                </div>
            </div>
            </div>
        </div>
</body>
</html>