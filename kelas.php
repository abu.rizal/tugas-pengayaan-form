<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>SIM Dosen</title>
</head>
<body>
    <?php 
        if (isset($_SESSION['message'])):
    ?>

    <div class="alert-<?=$_SESSION['msg_type']?>"></div>
    
    <?php 
        endif;
    ?>
<!-- Membuat Scaffold tampilan -->
<div class="wrapper">
            <div class="sidebar">
                <!-- Membuat tampilan menu sidebar -->
                <h2>SIM Dosen</h2>
                <hr>
                <ul>
                    <li><a href="index.php">Jadwal Kelas</a></li>
                    <li><a href="dosen.php">Dosen</a></li>
                    <li><a href="kelas.php">Kelas</a></li>
                </ul>
            </div>
            <div class="main_content">
                <div class="page_title">
                    KELAS <!-- Title Halaman -->
                </div>
                <div class="container">
                <div class="display">
                <?php
                    // Koneksi ke Database
                    $mysqli = new mysqli('localhost', 'root', '', 'db_sidosen') or die (mysqli_error($mysqli));

                    // Menyimpan hasil query ke dalam variabel
                    $result  = $mysqli->query("SELECT * FROM kelas") or die($mysqli->error);
                ?>

                <!-- Tabel untuk menampilkan hasil query (READ) -->
                <table class="table">
                    <thead>
                        <tr>
                            <th>ID Kelas</th>
                            <th>Nama Kelas</th>
                            <th>Prodi</th>
                            <th>Fakultas</th>
                            <th>Opsi</th>
                            
                        </tr>
                    </thead>

                    <?php
                        // perulangan untuk menampilkan isi database
                        while ($row = $result->fetch_assoc()): ?>
                        <tr>
                            <td><?php echo $row['id_kelas']; ?></td>
                            <td><?php echo $row['nama_kelas']; ?></td>
                            <td><?php echo $row['prodi']; ?></td>
                            <td><?php echo $row['fakultas']; ?></td>
                            <td colspan="2" class="action">
                                <!-- Tombol edit dan delete -->
                                <a href="kelas.php?edit=<?php echo $row['id_kelas']; ?>" class="edit">Sunting</a>
                                <a href="kelas.php?delete=<?php echo $row['id_kelas']; ?>" class="delete">Hapus</a>
                            </td>
                        </tr>
                        
                        
                        <?php 
                        endwhile; 
                        ?>

                        
                    
                </table>

                </div>

                 <!-- Form untuk input data ke database -->
                <div class="form">
                    <?php 
                        $update = false;
                        $id = 0; // variable id digunakan untuk menampung nilai id(PRIMARY) awal

                        // variable awalnya ditetapkan sebagai null, agar tampilan input kosong
                        $id_kelas = '';
                        $nama_kelas = '';
                        $prodi = '';
                        $fakultas = '';

                        // perkondisian untuk edit(UPDATE)
                        if (isset($_GET['edit'])){ // jika tombol edit/sunting ditekan,
                            $id_kelas = $_GET['edit']; // maka akan mengambil nilai dari id kemudian melakukan query:
                            $result = $mysqli->query("SELECT * FROM kelas WHERE id_kelas=$id_kelas") or die($mysqli->error);
                            
                            $update = true;
                            $row = mysqli_fetch_assoc($result);
                            
                            // perkondisian untuk menampilkan semua data dari data yg ditekan fungsi edit nya
                            // data kemudian ditampilkan ke form update
                            if($row['id_kelas'] > 0){
                                $id_kelas = $row['id_kelas'];
                                $nama_kelas = $row['nama_kelas'];
                                $prodi = $row['prodi'];
                                $fakultas = $row['fakultas'];
                                $id = $row['id_kelas'];
                            }
                        }

                        // perkondisian fungsi update
                        if (isset($_POST['update'])){
                            $id = $_POST['id'];
                            $id_kelas = $_POST['id_kelas'];
                            $nama_kelas = $_POST['nama_kelas'];
                            $prodi = $_POST['prodi'];
                            $fakultas = $_POST['fakultas'];
                            $mysqli->query("UPDATE kelas SET id_kelas='$id_kelas',nama_kelas='$nama_kelas',prodi='$prodi',fakultas='$fakultas' WHERE `kelas`.`id_kelas`=$id") or die($mysqli->error);

                            $_SESSION['message'] = "Data berhasil diperbarui!";
                            $_SESSION['msg_type'] = "info";

                            header("location: kelas.php");
                        }
                    ?>

                        <!-- Perkondisian untuk mengubah bentuk form dari submit form ke update form -->
                        <?php if($update == true): ?>
                        <h3>Ubah Data Kelas</h3>
                        <?php else: ?>
                        <h3>Data Kelas Baru</h3>
                        <?php endif; ?>
                    
                    <!-- Tampilan form -->
                    <form action="kelas.php" method= "POST">
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                        <label for="id_kelas">ID Kelas :</label><br>
                        <input type="text" name="id_kelas" value="<?php echo $id_kelas ?>"><br>

                        <label for="nama_kelas">Nama Kelas :</label><br>
                        <input type="text" name="nama_kelas" value="<?php echo $nama_kelas ?>"><br>

                        <label for="prodi">Prodi :</label><br>
                        <input type="text" name="prodi" value="<?php echo $prodi ?>"><br>

                        <label for="fakultas">Fakultas :</label><br>
                        <input type="text" name="fakultas" value="<?php echo $fakultas ?>"><br>
                        
                        <!-- Perkondisian untuk mengubah bentuk tombol dari submit ke update -->
                        <?php if($update == true): ?>
                            <input type="submit" name="update" value="Update">
                        <?php else: ?>
                        <input type="submit" name="submit" value="Submit">
                        <?php endif; ?>
                    </form>

                    <?php
                        // perkondisian untuk input data ke dalam Database (CREATE)
                        if (isset($_POST['submit'])){
                            $id_kelas = $_POST['id_kelas'];
                            $nama_kelas = $_POST['nama_kelas'];
                            $prodi = $_POST['prodi'];
                            $fakultas = $_POST['fakultas'];
                            

                            $mysqli->query("INSERT INTO kelas (id_kelas,nama_kelas,prodi,fakultas) VALUES ('$id_kelas','$nama_kelas','$prodi','$fakultas')") or
                                die($mysqli->error);

                            $_SESSION['message'] = "Data berhasil disimpan!";
                            $_SESSION['msg_type'] = "success";

                            header("location: kelas.php");
                        }

                        // perkondisian untuk menghapus data dari Database (DELETE)
                        if (isset($_GET['delete'])){
                            $id_kelas = $_GET['delete'];
                            $mysqli->query("DELETE FROM kelas WHERE id_kelas=$id_kelas") or die($mysqli->error);

                            $_SESSION['message'] = "Data berhasil dihapus!";
                            $_SESSION['msg_type'] = "danger";

                            header("location: kelas.php");
                        }
                    ?>
                </div>
            </div>
            </div>
        </div>
</body>
</html>