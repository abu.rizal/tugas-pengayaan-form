<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>SIM Dosen</title>
</head>
<body>
    <?php 
        if (isset($_SESSION['message'])):
    ?>

    <div class="alert-<?=$_SESSION['msg_type']?>"></div>
    
    <?php 
        endif;
    ?>
<!-- Membuat Scaffold tampilan -->
<div class="wrapper">
        <!-- Membuat tampilan menu sidebar -->
            <div class="sidebar">
                <h2>SIM Dosen</h2>
                <hr>
                <ul>
                    <li><a href="index.php">Jadwal Kelas</a></li>
                    <li><a href="dosen.php">Dosen</a></li>
                    <li><a href="kelas.php">Kelas</a></li>
                </ul>
            </div>
            <div class="main_content">
                <div class="page_title">
                    JADWAL KELAS <!-- Title Halaman -->
                </div>
                <div class="container">
                <div class="display">
                <?php
                    // Koneksi ke Database
                    $mysqli = new mysqli('localhost', 'root', '', 'db_sidosen') or die (mysqli_error($mysqli));

                    // Menyimpan hasil query ke dalam variabel
                    $result  = $mysqli->query("SELECT * FROM jadwal_kelas") or die($mysqli->error);
                ?>

                <!-- Tabel untuk menampilkan hasil query (READ) -->
                <table class="table">
                    <thead>
                        <tr>
                            <th>ID Jadwal</th>
                            <th>ID Dosen</th>
                            <th>ID Kelas</th>
                            <th>Jadwal</th>
                            <th>Mata Kuliah</th>
                            <th>Opsi</th>
                            
                        </tr>
                    </thead>

                    <?php
                        // perulangan untuk menampilkan isi database
                        while ($row = $result->fetch_assoc()): ?>
                        <tr>
                            <td><?php echo $row['id_jadwal']; ?></td>
                            <td><?php echo $row['id_dosen']; ?></td>
                            <td><?php echo $row['id_kelas']; ?></td>
                            <td><?php echo $row['jadwal']; ?></td>
                            <td><?php echo $row['mata_kuliah']; ?></td>
                            <td colspan="2" class="action">
                                <!-- Tombol edit dan delete -->
                                <a href="index.php?edit=<?php echo $row['id_jadwal']; ?>" class="edit">Sunting</a>
                                <a href="index.php?delete=<?php echo $row['id_jadwal']; ?>" class="delete">Hapus</a>
                            </td>

                        </tr>
                        <?php endwhile; ?>
                    
                </table>

                </div>

                <!-- Form untuk input data ke database -->
                <div class="form">
                    <?php

                        $update = false;
                        $id = 0; // variable id digunakan untuk menampung nilai id(PRIMARY) awal

                        // variable awalnya ditetapkan sebagai null, agar tampilan input kosong
                        $id_jadwal = '';
                        $id_dosen = '';
                        $id_kelas = '';
                        $jadwal = '';
                        $mata_kuliah = '';
                        
                        // perkondisian untuk edit(UPDATE)
                        if (isset($_GET['edit'])){ // jika tombol edit/sunting ditekan,
                            $id_jadwal = $_GET['edit']; // maka akan mengambil nilai dari id kemudian melakukan query:
                            $result = $mysqli->query("SELECT * FROM jadwal_kelas WHERE id_jadwal=$id_jadwal") or die($mysqli->error);
                            
                            $update = true; 
                            $row = mysqli_fetch_assoc($result);

                            // perkondisian untuk menampilkan semua data dari data yg ditekan fungsi edit nya
                            // data kemudian ditampilkan ke form update
                            if($row['id_jadwal'] > 0){
                                
                                $id_jadwal = $row['id_jadwal'];
                                $id_dosen = $row['id_dosen'];
                                $id_kelas = $row['id_kelas'];
                                $jadwal = $row['jadwal'];
                                $mata_kuliah = $row['mata_kuliah'];
                                $id = $row['id_jadwal']; // di sini id menyimpan nilai dari id(PRIMARY) lama
                            }
                        }

                        // perkondisian fungsi update
                        if (isset($_POST['update'])){
                            $id = $_POST['id'];
                            $id_jadwal = $_POST['id_jadwal'];
                            $id_dosen = $_POST['id_dosen'];
                            $id_kelas = $_POST['id_kelas'];
                            $jadwal = $_POST['jadwal'];
                            $mata_kuliah = $_POST['matkul'];
                            $mysqli->query("UPDATE jadwal_kelas SET id_jadwal='$id_jadwal',id_dosen='$id_dosen',id_kelas='$id_kelas',jadwal='$jadwal',mata_kuliah='$mata_kuliah' WHERE id_jadwal=$id") or die($mysqli->error);

                            $_SESSION['message'] = "Data berhasil diperbarui!";
                            $_SESSION['msg_type'] = "info";

                            header("location: index.php");
                        }
                    ?>

                    <!-- Perkondisian untuk mengubah bentuk form dari submit form ke update form -->
                    <?php if($update == true): ?>
                    <h3>Ubah Data Jadwal</h3>
                    <?php else: ?>
                    <h3>Data Jadwal Baru</h3>
                    <?php endif; ?>
                    
                    <!-- Tampilan form -->
                    <form action="index.php" method= "POST">
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                        <label for="id_jadwal">ID Jadwal :</label><br>
                        <input type="text" name="id_jadwal" value="<?php echo $id_jadwal ?>"><br>

                        <label for="id_dosen">ID Dosen :</label><br>
                        <input type="text" name="id_dosen" value="<?php echo $id_dosen ?>"><br>

                        <label for="id_kelas">ID Kelas :</label><br>
                        <input type="text" name="id_kelas" value="<?php echo $id_kelas ?>"><br>

                        <label for="jadwal">Jadwal :</label><br>
                        <input type="datetime-local" name="jadwal" value="<?php echo $jadwal ?>"><br>

                        <label for="matkul">Mata Kuliah :</label><br>
                        <input type="text" name="matkul" value="<?php echo $mata_kuliah ?>"><br>
                        
                        <!-- Perkondisian untuk mengubah bentuk tombol dari submit ke update -->
                        <?php if($update == true): ?>
                            <input type="submit" name="update" value="Update">
                        <?php else: ?>
                        <input type="submit" name="submit" value="Submit">
                        <?php endif; ?>
                    </form>

                    <?php

                        // perkondisian untuk input data ke dalam Database (CREATE)
                        if (isset($_POST['submit'])){
                            $id_jadwal = $_POST['id_jadwal'];
                            $id_dosen = $_POST['id_dosen'];
                            $id_kelas = $_POST['id_kelas'];
                            $jadwal = $_POST['jadwal'];
                            $mata_kuliah = $_POST['matkul'];
                            

                            $mysqli->query("INSERT INTO jadwal_kelas (id_jadwal,id_dosen,id_kelas,jadwal,mata_kuliah) VALUES ('$id_jadwal','$id_dosen','$id_kelas','$jadwal','$mata_kuliah')") or
                                die($mysqli->error);

                            $_SESSION['message'] = "Data berhasil disimpan!";
                            $_SESSION['msg_type'] = "success";

                            header("location: index.php");
                        }

                        // perkondisian untuk menghapus data dari Database (DELETE)
                        if (isset($_GET['delete'])){
                            $id_jadwal = $_GET['delete'];
                            $mysqli->query("DELETE FROM jadwal_kelas WHERE id_jadwal=$id_jadwal") or die($mysqli->error);

                            $_SESSION['message'] = "Data berhasil dihapus!";
                            $_SESSION['msg_type'] = "danger";

                            header("location: index.php");
                        }
                    ?>
                </div>
            </div>
            </div>
        </div>
</body>
</html>